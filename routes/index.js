var express = require('express');
var router = express.Router();
// ADDITIONAL PLUGINS FOR PAYPAL
const axios = require('axios').default;
const qs = require('qs');

var paypal = require('paypal-rest-sdk');

let baseUrl = 'https://www.sandbox.paypal.com/';
let client_id = `COPY FROM SANDBOX ACCOUNT`;
let client_secret = `COPY FROM SANDBOX ACCOUNT`;
let scope = 'openid profile email address';
let redirect_uri = 'http://127.0.0.1:3000/payments/paypal/callback'

// OpenID configuration
paypal.configure({
  'openid_client_id': client_id,
  'openid_client_secret': client_secret,
  'openid_redirect_uri': redirect_uri });

router.get('/:userID/payments/paypal', function(req, res, next) {
  // res.redirect(`${baseUrl}connect/?flowEntry=static&client_id=${client_id}&response_type=code&scope=${scope}&redirect_uri=${redirect_uri}&state=${req.params.userID}`);
  res.redirect(paypal.openIdConnect.authorizeUrl({'scope': 'openid profile', state: `{"influencerId":"${req.params.userID}"}`}))

});

router.get('/payments/paypal/callback', async function(req, res, next) {
  console.log('============= REQUEST PARAMS')
  console.log(req.query);
  console.log(JSON.parse(req.query.state))
paypal.openIdConnect.tokeninfo.create(req.query.code, function(error, tokeninfo){
  console.log(tokeninfo);
  // Get userinfo with Access code
paypal.openIdConnect.userinfo.get(tokeninfo.access_token, function(error, userinfo){
  console.log(userinfo);
  res.send(userinfo);
});
});


});





/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
